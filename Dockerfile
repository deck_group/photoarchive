FROM python:3.6
ENV PYTHONUNBUFFERED 1
WORKDIR /src
ADD . /src
COPY ./requirements.txt /src/requirements.txt
RUN pip install -r requirements.txt
CMD ["python", "manage.py", "runserver", "0.0.0.0:8080"]